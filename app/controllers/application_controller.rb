class ApplicationController < ActionController::Base
  protect_from_forgery _with: :exception

  def Hello
    render html: "hello"
  end
end
